<?php

namespace App\Providers;

use App\Services\Contracts\RandomGenerator;
use App\Services\RandomGeneratorService;
use Illuminate\Support\ServiceProvider;

class RandomGeneratorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(RandomGenerator::class, RandomGeneratorService::class);
    }
}
