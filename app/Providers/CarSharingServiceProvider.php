<?php

namespace App\Providers;

use App\Services\CarSharingService;
use App\Services\Contracts\CarSharing;
use Illuminate\Support\ServiceProvider;

class CarSharingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CarSharing::class, CarSharingService::class);
    }
}
