<?php

namespace App\Services;

use App\Repositories\CarRepository;
use App\Repositories\Contracts\Repository;
use App\Services\Contracts\CarSharing;
use App\Services\Contracts\RandomGenerator;

class CarSharingService implements CarSharing
{
    private $repository;
    private $randomGenerator;

    public function __construct(Repository $repository, RandomGenerator $randomGenerator)
    {
        $this->repository = $repository;
        $this->randomGenerator = $randomGenerator;
    }

    public function getAllCars()
    {
        return $this->repository->all();
    }

    public function getRandomCar()
    {
        $carCollection = $this->repository->all();
        $length = count($carCollection);
        $randomIndex = $this->randomGenerator->getRandomInt(0, $length - 1);

        return $carCollection[$randomIndex];
    }
}